/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.entities;

/**
 *
 * @author Sandali Kaushalya
 */
public class BirthdayTemplate {
    
   private int id;
   private String templateSystemId;
    private String title;
    private String icon;
    private String templateMessage;
    
    private int status;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTemplateSystemId() {
        return templateSystemId;
    }

    public void setTemplateSystemId(String templateSystemId) {
        this.templateSystemId = templateSystemId;
    }
    
    

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setTemplateMessage(String templateMessage) {
        this.templateMessage = templateMessage;
    }

    public String getTemplateMessage() {
        return templateMessage;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
