/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controller;

import com.dmssw.entities.BirthdayTemplate;
import com.dmssw.entities.UserDetails;
import com.dmssw.majlis.config.AppParams;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisBirthdayTemplate;
import com.dmssw.orm.models.MajlisMdCode;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.hibernate.Session;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import java.text.SimpleDateFormat;

/**
 *
 * @author Hashan Jayakody
 * @version 1.0
 */
@LocalBean
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BirthdayTemplateController {

    private static Logger LOGGER = Logger.getLogger("InfoLogging");

    private org.apache.log4j.Logger logger;

    @Resource
    UserTransaction transaction;

    public BirthdayTemplateController() {

        logger = com.dmssw.orm.config.AppParams.logger;
    }

    /**
     * Insert a new birthday template record to the database.
     *
     * @param majlisBirthdayTemplate
     * @return
     */
    public ResponseData createBirthdayTemplate(MajlisBirthdayTemplate majlisBirthdayTemplate) {

        ResponseData responseData = new ResponseData();
        int result = -1;
        String imgPath = null;

        try {
            ORMConnection connection = new ORMConnection();
            Session session = connection.beginTransaction();

            int statusId = 14;
            MajlisMdCode mdCode = new MajlisMdCode(statusId);// Default Value
            majlisBirthdayTemplate.setTemplateStatus(mdCode);

            if ((majlisBirthdayTemplate.getTemplateTitle() != null) && (majlisBirthdayTemplate.getTemplateMessage() != null)
                    && (majlisBirthdayTemplate.getTemplateStatus() != null)) {

                int templateId = connection.createObject(session, majlisBirthdayTemplate);

                if (majlisBirthdayTemplate.getTemplateIconPath() != null) {
                    String url = majlisBirthdayTemplate.getTemplateIconPath();
                    System.out.println("URL" + url);
                    String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");

                    imgPath = arrayUrl[1];
                } else {
                    imgPath = AppParams.IMG_PATH_TEMPLATE + "/" + "image.jpg";
                    imgPath = imgPath.substring(1);

                }

                majlisBirthdayTemplate.setTemplateIconPath(imgPath);
                majlisBirthdayTemplate.setTemplateId(templateId);
                majlisBirthdayTemplate.setTemplateSystemId(generateSystemId(templateId));

                Date date = new Date();
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm:ss ");
//                String formattedDate = sdf.format(date);

                majlisBirthdayTemplate.setDateInserted(date);

                connection.updateObject(session, majlisBirthdayTemplate);
                connection.commitObject(session);
                result = 1;
                responseData.setResponseData("SUCESSS");
                session.close();
            } else {
                result = 999;
                logger.info("Entries are null...");
            }

        } catch (Exception ex) {
            result = 999;
            logger.info("Exception occur...");
            ex.printStackTrace();
        }
        responseData.setResponseCode(result);
        return responseData;
    }

    /**
     * Update a majlis birthday template record in a the database for a given
     * birthday template
     *
     * @param majlisBirthdayTemplate
     * @return
     */
    public ResponseData updateBirthdayTemplate(MajlisBirthdayTemplate majlisBirthdayTemplate) {

        ResponseData responseData = new ResponseData();
        int result = -1;
        String imgPath = null;

        try {
            ORMConnection connection = new ORMConnection();
            Session session = connection.beginTransaction();

            Integer templateId = majlisBirthdayTemplate.getTemplateId();

            if ((templateId != null) && (majlisBirthdayTemplate.getTemplateMessage() != null)
                    && (majlisBirthdayTemplate.getTemplateTitle() != null) && (majlisBirthdayTemplate.getTemplateMessage() != null)) {

                if (majlisBirthdayTemplate.getTemplateIconPath() != null) {
                    String url = majlisBirthdayTemplate.getTemplateIconPath();
                    String[] arrayUrl = url.split(AppParams.MEDIA_SERVER + "/");

                    imgPath = arrayUrl[1];
                } else {
                    imgPath = AppParams.IMG_PATH_TEMPLATE + "/" + "image.jpg";
                    imgPath = imgPath.substring(1);

                }

                majlisBirthdayTemplate.setTemplateIconPath(imgPath);

                connection.updateObject(session, majlisBirthdayTemplate);
                connection.commitObject(session);
                result = 1;
                responseData.setResponseData(majlisBirthdayTemplate);

                session.close();

            } else {
                result = 999;
                logger.info("Null  entries");
            }

        } catch (Exception ex) {

            result = 999;
            logger.info("Exception...");
            ex.printStackTrace();
        }

        responseData.setResponseCode(result);
        return responseData;

    }

    /**
     * Returns a list of birthday templates.
     *
     * @param start -
     * @param limit
     * @return
     */
    public ResponseData getBirthdayTemplateList(int start, int limit) {
        ResponseData responseData = new ResponseData();
        int result = -1;
        List<MajlisBirthdayTemplate> majlisTemplateList = new ArrayList<MajlisBirthdayTemplate>();
        MajlisBirthdayTemplate majlisBirthDayTemplate = new MajlisBirthdayTemplate();
        List<Object> list;

        try {
            ORMConnection connection = new ORMConnection();
            String hql = "SELECT m FROM MajlisBirthdayTemplate m";

            list = connection.hqlGetResultsWithLimit(hql, start, limit);

            for (Object obj : list) {
                majlisBirthDayTemplate = (MajlisBirthdayTemplate) obj;
                majlisBirthDayTemplate.setTemplateIconPath(validateGroupIconPath(majlisBirthDayTemplate.getTemplateIconPath()));
                majlisTemplateList.add(majlisBirthDayTemplate);

            }

            responseData.setResponseData(majlisTemplateList);
            result = 1;

        } catch (Exception ex) {
            result = 999;
            logger.info("Exception");
            ex.printStackTrace();
        }

        responseData.setResponseCode(result);
        return responseData;

    }

    /**
     * Returns a list of birthday template details for a specific template
     * id(unique id).
     *
     * @param templateId
     * @param start
     * @param limit
     * @return template detail list
     */
    public ResponseData getTemplateDetail(int templateId, int start, int limit) {
        ResponseData responseData = new ResponseData();
        int result = -1;
        List<MajlisBirthdayTemplate> majlisTemplateList = new ArrayList<MajlisBirthdayTemplate>();
        MajlisBirthdayTemplate majlisBirthDayTemplate = new MajlisBirthdayTemplate();
        List<Object> list;

        try {
            ORMConnection connection = new ORMConnection();

            String tail = templateId == 0 ? "m.templateId =m.templateId " : "m.templateId = " + templateId;
            String hql = "SELECT m FROM MajlisBirthdayTemplate m WHERE " + tail;

            list = connection.hqlGetResultsWithLimit(hql, start, limit);

            for (Object obj : list) {
                majlisBirthDayTemplate = (MajlisBirthdayTemplate) obj;
                majlisBirthDayTemplate.setTemplateIconPath(validateGroupIconPath(majlisBirthDayTemplate.getTemplateIconPath()));
                majlisTemplateList.add(majlisBirthDayTemplate);

            }

            responseData.setResponseData(majlisTemplateList);
            result = 1;
        } catch (Exception ex) {
            result = 999;
            logger.info("Exception");
            ex.printStackTrace();
        }

        responseData.setResponseCode(result);
        return responseData;

    }

    /**
     * Generated a system ID from current date. SystemId(YYYYMM_Serial – Auto
     * Generate)
     *
     * @param templateId-Auto generated templateId
     * @return generated system ID.
     */
    public String generateSystemId(int templateId) {
        String systemId;
        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        systemId = year + month + templateId + "";
        return systemId;

    }

    /**
     *
     * @param base64Img
     * @param groupId
     * @return
     */
    private String convertIconPath(String base64Img, int templateId) {
        String iconPath = null;

        if ((base64Img != null) && (base64Img.length() > 0)) {

            try {

                byte[] decodedBytes = Base64.getMimeDecoder().decode(base64Img);
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedBytes));

                if (bufferedImage != null) {

                    String path = "Majlis_images/birthdaytemplate/" + templateId + "_BDayTemplateIcons" + ".jpg";
                    iconPath = "/" + path;
                    path = path.replace("\\", "/");

                    File f = new File(AppParams.IMG_PATH + path);
                    ImageIO.write(bufferedImage, "png", f);

                } else {
                    LOGGER.info("NO image to write");

                }

            } catch (IOException e) {

                LOGGER.info("Error in Streaming...");
                e.printStackTrace();
            }

        } else {
            LOGGER.info("Invalid Image...");
        }

        return iconPath;

    }

    /**
     * Returns the absolute path to the group icon.
     *
     * @param path- the relative path recorded in the database
     * @return image url to the server's path
     */
    private String validateGroupIconPath(String path) {

        String ipath = "";
        if (path != null) {

            return AppParams.MEDIA_SERVER + "/" + path;

        }
        return ipath;
    }

    /**
     * Upload the template icon image to the server and returns the image url
     *
     * @param input
     * @return image url to the server's path
     */
    public ResponseData saveTemplateIcon(MultipartFormDataInput input) {

        int result = -1;
        ResponseData rd = new ResponseData();

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

        try {

            logger.info("start saveGroupIcon method............");

            transaction.begin();

            List<InputPart> inputParts = uploadForm.get("file");

            for (InputPart inputPart : inputParts) {

                MultivaluedMap<String, String> headers = inputPart.getHeaders();
                try {

                    java.io.InputStream inputStream = inputPart.getBody(java.io.InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    String saveLocation = AppParams.IMG_PATH;

                    Random rnd = new Random();

                    int randomId = 100000 + rnd.nextInt(900000);

                    String pathForDb = com.dmssw.majlis.config.AppParams.IMG_PATH_TEMPLATE + "/" + randomId + ".jpg";

                    File dir = new File(saveLocation + com.dmssw.majlis.config.AppParams.IMG_PATH_TEMPLATE);

                    dir.mkdir();
                    logger.info("image path is " + saveLocation);

                    FileOutputStream stream = new FileOutputStream(saveLocation + pathForDb);

                    try {
                        stream.write(bytes);
                    } finally {
                        stream.close();
                    }

                    result = 1;
                    rd.setResponseData(AppParams.MEDIA_SERVER + pathForDb);
                    transaction.commit();
                } catch (IOException e) {
                    transaction.rollback();
                    result = 999;
                    logger.error("Cannot save the image " + e.getMessage());

                }
            }

        } catch (Exception ex) {

            logger.error("EJB Exception " + ex.getMessage());

        }
        rd.setResponseCode(result);

        return rd;

    }

    ////////////////////////////////////////Sandali//////////////////////////////////////////
    public static ResponseData getActivatedBirthdayTemplate() {

        ResponseData rd = new ResponseData();
        int result = -1;
        int i = 0;

        DbCon db = new DbCon();
        Connection conn = db.getCon();

        BirthdayTemplate template = new BirthdayTemplate();

        try {

            String selectQuery = "Select t.TEMPLATE_ID, t.TEMPLATE_SYSTEM_ID, t.TEMPLATE_TITLE, t.TEMPLATE_ICON_PATH, t.TEMPLATE_MESSAGE, t.TEMPLATE_STATUS, t.DATE_INSERTED, t.USER_INSERTED, t.DATE_MODIFIED, t.USER_MODIFIED"
                    + " from majlis_birthday_template t"
                    + " where t.TEMPLATE_STATUS= (Select  c.CODE_ID"
                    + " from majlis_md_code c"
                    + " where c.CODE_TYPE='BIRTHDAY' AND c.CODE_SUB_TYPE='ACTIVE_STATUS' "
                    + "AND c.CODE_MESSAGE ='Active' AND c.CODE_LOCALE='" + AppParams.LOCALE + "')";

            System.out.println("selectQuery:" + selectQuery);

            ResultSet rs = db.search(conn, selectQuery);
            result = 1;
            while (rs.next()) {

                template.setId(rs.getInt("TEMPLATE_ID"));
                template.setTemplateSystemId(rs.getString("TEMPLATE_SYSTEM_ID"));
                template.setTitle(rs.getString("TEMPLATE_TITLE"));
                template.setIcon(rs.getString("TEMPLATE_ICON_PATH"));
                template.setTemplateMessage(rs.getString("TEMPLATE_MESSAGE"));

            }

            rd.setResponseCode(result);
            rd.setResponseData(template);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(BirthdayTemplateController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return rd;
    }

    public static List<UserDetails> checkBirthdayUsers() {

        List<UserDetails> birthdayUsers = new ArrayList<>();

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        try {

            rd = getActivatedBirthdayTemplate();

            String selectUsers = "select m.MOBILE_USER_ID, m.MOBILE_USER_NAME, m.MOBILE_USER_EMAIL"
                    + " from majlis_mobile_users m "
                    + " where m.MOBILE_USER_DOB = CURDATE()";

            ResultSet rs = db.search(conn, selectUsers);
            flag = 1;

            while (rs.next()) {
                int userId = rs.getInt("MOBILE_USER_ID");
                String email = rs.getString("MOBILE_USER_EMAIL");
                String userName = rs.getString("MOBILE_USER_NAME");
                System.out.println("user id" + userId);

                System.out.println("before send mail");
                boolean b = EmailSender.sendHTMLMail(email, userName, rd);
                System.out.println("boolean:" + b);

            }

        } catch (Exception e) {

            e.printStackTrace();
            flag = -1;
            rd.setResponseCode(flag);

        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(BirthdayTemplateController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return birthdayUsers;

    }

    public ResponseData activation(int templateId) {

        ResponseData responseData = new ResponseData();
        int result = -1;
        boolean isUpdated = false;

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {
            String updateAll = "UPDATE majlis_birthday_template SET TEMPLATE_STATUS = 14 WHERE TEMPLATE_STATUS = 13";
            dbCon.save(connection, updateAll);
            isUpdated = true;
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();

        }
        if (isUpdated) {
            String updateNew = "UPDATE majlis_birthday_template SET "
                    + "TEMPLATE_STATUS = 13 WHERE TEMPLATE_ID =" + templateId;
            try {
                dbCon.save(connection, updateNew);
                result = 1;
                responseData.setResponseData("Success");
            } catch (Exception ex) {
                result = 999;
                Logger.getLogger(BirthdayTemplateController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(BirthdayTemplateController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {

            try {
                result = 999;
                logger.info("Error Updated ALL");
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(BirthdayTemplateController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        responseData.setResponseCode(result);
        return responseData;
    }

}
