/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.service;

import com.dmssw.majlis.controller.BirthdayTemplateController;
import com.dmssw.orm.models.MajlisBirthdayTemplate;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author Hashan Jayakody
 */
@Path("/birthdaytemplate")
public class BirthdayTemplateService {

    @EJB
    BirthdayTemplateController birthdayTemplateController;

    /**
     * Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/createBirthDayTemplate
     * @param majlisBirthDayTemplate
     * @return 
     */
    @POST
    @Path("/createBirthDayTemplate")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createBirthDayTemplate(MajlisBirthdayTemplate majlisBirthDayTemplate) {
            
        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(birthdayTemplateController.createBirthdayTemplate(majlisBirthDayTemplate)).build();
    }

    /**
     * Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/updateBirthDayTemplate
     * @param majlisBirthDayTemplate
     * @return 
     */
    @POST
    @Path("/updateBirthDayTemplate")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateBirthDayTemplate(MajlisBirthdayTemplate majlisBirthDayTemplate) {
            
        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(birthdayTemplateController.updateBirthdayTemplate(majlisBirthDayTemplate)).build();
    }
    
    /**
     *  Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/getTemplateList
     * @param start
     * @param limit
     * @return 
     */
    @GET
    @Path("/getTemplateList")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    
    public Response getTemplateList(@QueryParam("start") @DefaultValue("0") int start, @QueryParam("limit") @DefaultValue("25") int limit){
        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(birthdayTemplateController.getBirthdayTemplateList(start, limit)).build();
    
    }
    /**
     * Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/getTemplateDetail
     * @param templateId
     * @param start
     * @param limit
     * @return 
     */
    @GET
    @Path("/getTemplateDetail")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    
    public Response getTemplateDetail(@QueryParam("templateId") @DefaultValue("*") int templateId, @QueryParam("start") @DefaultValue("0") int start, 
            @QueryParam("limit") @DefaultValue("25") int limit){
        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(birthdayTemplateController.getTemplateDetail(templateId, start, limit)).build();
    
    }
    
    /**
     * Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/activation
     * @param templateId
     * @param start
     * @param limit
     * @return 
     */
    @GET
    @Path("/activation")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    
    public Response enableactivel(@QueryParam("templateId") @DefaultValue("0") int templateId){
        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(birthdayTemplateController.activation(templateId)).build();
    
    }
    
    /**
     * Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/saveTemplateIcon
     * @param input
     * @param headers
     * @return 
     */
    @POST
    @Path("/saveTemplateIcon")
    @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveTemplateIcon(MultipartFormDataInput input, @Context HttpHeaders headers) {

        headers.getRequestHeaders().forEach((key, val) -> {

            for (String string : val) {
                System.out.println("key " + key + " val... " + val);
            }

        });


        return Response.status(Response.Status.OK).entity(birthdayTemplateController.saveTemplateIcon(input)).build();
    }
    

}
