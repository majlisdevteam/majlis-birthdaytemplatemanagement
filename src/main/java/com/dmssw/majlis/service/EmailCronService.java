/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.service;


import com.dmssw.majlis.controller.BirthdayTemplateController;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Sandali Kaushalya
 */
@Singleton
@Startup
public class EmailCronService {
       @Schedule(second = "0", minute = "30", dayOfWeek = "*", hour = "17", persistent = false)
    public void cronJob() {
       
           System.out.println("checked");
        BirthdayTemplateController.checkBirthdayUsers();
        

    }
}
